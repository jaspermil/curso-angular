import { Component, OnInit } from '@angular/core';
import { DestinosApiClient } from 'src/app/models/destino-viaje/destinos-api-client.model';
import { DestinoViaje } from "src/app/models/destino-viaje/destino-viaje.model";
import { ActivatedRoute } from '@angular/router';
//import { AppState } from 'src/app/app.module';
//import { Store } from '@ngrx/store';
/*
class DestinosApiClientViejo {
  getById( id: String ): DestinoViaje {
    console.log('Llamado por la clase vieja!');
    return null;
  }
}
//--------------------------------------------------------------------------------------------------------
interface AppConfig {
  apiEndpoint: String;
}

const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'mi_api.com'
};

const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
 
  class DestinoApiClientDecorated extends DestinosApiClient {
    constructor (@Inject(APP_CONFIG) private config: AppConfig, store: Store<AppState>){
      super(store);
    }

    getById(id: String): DestinoViaje{
      console.log('Llamado por la clase decorada!');
      console.log('config: ' + this.config.apiEndpoint);
      return super.getById(id);
  }

}*/
//--------------------------------------------------------------------------------------------------------
@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: [],
  //providers: [DestinosApiClient, { provide: DestinosApiClientViejo, useExisting: DestinosApiClient } ]
  //--------------------------------------------------------------------------------------------------------
  providers: [ DestinosApiClient ]
  //-------------------------------------------------------------------------------------------------------
})
export class DestinoDetalleComponent implements OnInit {

  destino: DestinoViaje;

  constructor( private route: ActivatedRoute, private destinosApiClient: DestinosApiClient ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    //this.destino = null; 
    this.destino = this.destinosApiClient.getById(id);
    

      /*this.route.params.subscribe(params => {
      //this.destino = this.destinosApiClient.getById( params['id'] );
      // console.log(this.heroe);
  });*/

  }

}
