import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje/destino-viaje.model';
import { DestinosApiClient } from 'src/app/models/destino-viaje/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from 'src/app/models/destino-viaje/destino-viajes-state-model';



@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit { 
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  //all;

  constructor( private destinosApiClient:DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      if (d!=null) {
        this.updates.push('Se ha elegido a ' + d.nombre);
        //console.log('Se ha elegido a');
      }
    });
    //store.select(state => state.destinos.items).subscribe(items => this.all = items);
    //this.destinosApiClient.subscribeOnChange(( d: DestinoViaje ) => {});
   }

  ngOnInit() {

    /*this.store.select(state => state.destinos.favorito)
    .subscribe(d => {
      const f = d;
      if ( f !=null ){
        this.updates.push('Se ha elegido a ' + d.nombre);
      }
    });*/
  }

  agregado( d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    this.store.dispatch(new NuevoDestinoAction(d));

  }
/*
  elegido( e: DestinoViaje ) {
    this.destinosApiClient.getAll().forEach(x => x.setSelected(false));
    e.setSelected( true );
    
  }*/

  //Destinos API Client
  elegido( e: DestinoViaje ) {
    this.destinosApiClient.elegir( e );
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }

  getAll() {

  }

}
