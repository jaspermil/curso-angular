import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from 'src/app/models/destino-viaje/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})

export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  /* FormGroup tenemos que inicializarlo, representa el numero de elementos 
  del formulario ya construido*/
  fg: FormGroup; 

  //Variable de validación
  minLongitud = 3;
  searchResults: string[];
  
  
  /* El FormBuilder nos permite construir el formulario*/
  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();
    /* inicializacion de nuestro formulario*/
    this.fg =fb.group({ //fb.group objeto de inializacion que define la estructura del formulario
      nombre: ['', Validators.compose([  // compose -> primitiva para componer un array de validadores
        Validators.required,
        this.nombreValidator,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],      //Validators -> validacion por defecto de angular
      url: ['', Validators.required]
    });
    //registrar un observable
    this.fg.valueChanges.subscribe((form: any) => {
      console.log("cambio el formulario:", form);
    });
  }

  ngOnInit() {
//Usar el texto del input y usarlo para buscar sugerencias
    const elemName = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemName, 'input')
    .pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 3),
      debounceTime(120),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
   }

  guardar(nombre: string, url: string): boolean{
    const d = new DestinoViaje(nombre,url);
    this.onItemAdded.emit(d);
    return false;
  }

  //Logica dw la validación personalizada
  nombreValidator( control:FormControl ): {[ s:string ]: boolean} {  // indica que devolveremos un objeto con ese formato
    const l = control.value.toString().trim().length;

    if ( l > 0 && l < 5 ) {
      return { invalidName: true };
    }
    return null;
  }

  //Validador parametrizable
  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): {[ s: string]: boolean} | null => {
      const l = control.value.toString().trim().length;
      if ( l > 0 && l < minLong ) {
        return { minLongName: true };
      }
      return null;
    }
    
  }


}
