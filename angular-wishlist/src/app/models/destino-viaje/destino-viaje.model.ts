import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    /*
    nombre: string;
    imagenUrl: string;

    constructor ( n:string, u:string ){
        this.nombre = n;
        this.imagenUrl = u;
    }*/

    //Declararlo como elegido

    private selected: boolean;
    public servicios: string[];
    id = uuid();

    constructor ( public nombre:string, public imagenUrl: string, public votes: number = 0 ){
        this.servicios = ['desayuno', 'piscina'];
    }
    //Metodos
    isSelected(): boolean {
        return this.selected; //Metodo para identificar si esta seleccionado o no
    }
    //Marcarlo como seleccionado - encapsular el acceso a dicha variable
    setSelected( s:boolean ){
        this.selected = s;
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }
}
